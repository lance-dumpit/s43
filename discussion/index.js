//alert("Hello World")

let posts = [];
let count = 1;

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();//prevent the page from loading

	//push to array
	posts.push({
		id: count, //will be coming from the count var
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	count++;//to increment the value of var count

	showPosts(posts);//showPosts is a function and created an argument post array
	alert('Successfully Added!');
});


//comes from the showPosts
const showPosts = (posts) => {
	let postEntries = '';

	//forEach is used to print
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>

		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Editing a Post

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;


	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}


//Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();


	for(let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){

			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;


			showPosts(posts);
			alert('Successfully updated');

			break;
		}
	}
});


// Delete Post
const deletePost = (id) => {

	posts = posts.filter(post => post.id.toString() !== id);//filter to remove elements from Array
	
	alert(`Successfully Deleted`)
	showPosts(posts)
}